﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public interface IArtikel
    {
        string Identnummer { get; set; }
        string Artikelnummer { get; set; }
        string Produktgruppe { get; set; }
        string Bezeichnung { get; set; }
        string Bezeichnung2 { get; set; }
        Nullable<decimal> Preis { get; set; }
        string Einheit { get; set; }
    }
}
