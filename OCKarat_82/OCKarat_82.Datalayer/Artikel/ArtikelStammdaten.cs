﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public class ArtikelStammdaten : IArtikelStammdaten
    {
        OCKaratEntities ctx = new OCKaratEntities();

        public bool CreateArtikel(F00S1201 artikel)
        {
            try
            {
                ctx.F00S1201.AddOrUpdate(artikel);
                ctx.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        public bool DeleteArtikelById(string artikelId)
        {
            var item = (from a in ctx.F00S1201
                        where a.Identnr == artikelId
                        select a).FirstOrDefault();

            try
            {
                if (item != null)
                {
                    ctx.F00S1201.Remove(item);
                }
                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        public F00S1201 GetArtikelById(string artikelId)
        {
            var item = (from a in ctx.F00S1201
                        where a.Identnr == artikelId
                        select a).FirstOrDefault();

            return item;
        }

        public F00S1201 GetFullArtikelById(string artikelId)
        {
            var item = (from a in ctx.F00S1201
                where a.Identnr == artikelId
                select a).FirstOrDefault();

            return item;
        }

        public List<Artikel> GetArtikel()
        {
            List<Artikel> artikel = ctx.F00S1201.OrderBy(x => x.ArtNr).Select(x => new Artikel
            {
                Identnummer = x.Identnr,
                Artikelnummer = x.ArtNr,
                Produktgruppe = x.HerCo,
                Bezeichnung = x.ArtBez,
                Bezeichnung2 = x.ArtBez2,
                Preis = x.PreisDM,
                Einheit = x.ME
            }).ToList();

            return artikel;
        }

        public bool UpdateArtikel(F00S1201 artikel)
        {
            try
            {
                ctx.F00S1201.AddOrUpdate(artikel);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public List<ArtikelLieferant> GetLieferantenByIdentNr(string identNr)
        {
            List<ArtikelLieferant> al = ctx.A13S0202.OrderBy(x => x.LIEF).Where(x => x.IDENTNR == identNr).Select(x => new ArtikelLieferant
            {
                Lieferant = x.LIEF,
                Match = x.Match,
                LieferantNr = x.ARTIKELLIEF,
                EkPreis = x.EK_PREIS,
                Rabatt = x.RABATT,
                NettoPreis = x.NETTO_EK,
                VkAufschlag = x.VK_Aufschlag,
                PreisGueltigkeit = x.PRGUELT_DATUM
            }).ToList();

            return al;
        }

        public List<Artikel> GetErsatzteile()
        {
            List<Artikel> artikel = ctx.F00S1201.Where(x => x.KZ_Art == "E").OrderBy(x => x.ArtNr).Select(x => new Artikel
            {
                Identnummer = x.Identnr,
                Artikelnummer = x.ArtNr,
                Produktgruppe = x.HerCo,
                Bezeichnung = x.ArtBez,
                Bezeichnung2 = x.ArtBez2,
                Preis = x.PreisDM,
                Einheit = x.ME
            }).ToList();

            return artikel;
        }
    }
}
