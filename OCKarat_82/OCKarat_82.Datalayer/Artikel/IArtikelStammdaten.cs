﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OCDatalayer
{
    public interface IArtikelStammdaten
    {
        List<Artikel> GetArtikel();

        F00S1201 GetArtikelById(string artikelID);

        bool CreateArtikel(F00S1201 artikel);

        bool UpdateArtikel(F00S1201 artikel);

        bool DeleteArtikelById(string artikelID);
    }
}
