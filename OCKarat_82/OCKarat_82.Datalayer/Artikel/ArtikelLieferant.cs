﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public class ArtikelLieferant
    {
        public string Lieferant { get; set; }
        public string Match { get; set; }
        public string LieferantNr { get; set; }
        public decimal? EkPreis { get; set; }
        public decimal? Rabatt { get; set; }
        public decimal? NettoPreis { get; set; }
        public DateTime? PreisGueltigkeit { get; set; }
        public decimal? VkAufschlag { get; set; }
    }

}
