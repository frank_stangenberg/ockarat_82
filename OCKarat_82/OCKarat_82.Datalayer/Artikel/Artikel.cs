﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public class Artikel : IArtikel
    {
        public string Identnummer { get; set; }
        public string Artikelnummer { get; set; }
        public string Produktgruppe { get; set; }
        public string Bezeichnung { get; set; }
        public string Bezeichnung2 { get; set; }
        public Nullable<decimal> Preis { get; set; }
        public string Einheit { get; set; }
    }
}
