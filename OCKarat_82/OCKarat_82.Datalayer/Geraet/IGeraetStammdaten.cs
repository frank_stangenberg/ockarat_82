﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OCDatalayer
{
    public interface IGeraetStammdaten
    {
        List<Geraet> GetGeraete();

        Geraet GetGeraetByIdentNr(string identNr);

        Geraet GetGeraetByCharge(string charge);

        bool CreateGeraet(Geraet geraet);

        bool UpdateGeraet(Geraet geraet);

        bool DeleteGeraetById(string identNr);
    }
}
