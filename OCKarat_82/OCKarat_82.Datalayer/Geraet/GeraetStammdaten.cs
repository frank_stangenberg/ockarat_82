﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace OCDatalayer
{
    public class GeraetStammdaten : IGeraetStammdaten
    {
        OCKaratEntities ctx = new OCKaratEntities();

        public bool CreateGeraet(Geraet geraet)
        {
            try
            {
                M13S0402 toInsert = Mapper.Map<M13S0402>(geraet);
                ctx.M13S0402.Add(toInsert);

                ctx.SaveChanges();

                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw;
            }
        }

        public bool UpdateGeraet(Geraet geraet)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// liefert das Gerät zur gegenen identNr
        /// </summary>
        /// <param name="identNr"></param>
        /// <returns>Geraet, sofern es gefunden wurde</returns>
        public Geraet GetGeraetByIdentNr(string identNr)
        {
            var selection = (from a in ctx.M13S0402
                          where a.IdentNr == identNr
                          select a).FirstOrDefault();

            Geraet geraet = new Geraet();
            geraet = Mapper.Map(selection, geraet);

            return geraet;
        }

        /// <summary>
        /// liefert ein Gerät zur gegenen charge
        /// </summary>
        /// <param name="charge"></param>
        /// <returns>Gerate, sofern es gefunden wurde</returns>
        public Geraet GetGeraetByCharge(string charge)
        {
            var selection = (from a in ctx.M13S0402
                          where a.charge == charge
                          select a).FirstOrDefault();

            Geraet geraet = new Geraet();
            geraet = Mapper.Map(selection, geraet);

            return geraet;
        }

        /// <summary>
        /// löscht das Gerät zur gegebenen identnr
        /// </summary>
        /// <param name="identNr"></param>
        /// <returns>true, löschen erfolgreich, false sonst</returns>
        public bool DeleteGeraetById(string identNr)
        {
            var geraet = (from a in ctx.M13S0402
                        where a.IdentNr == identNr
                        select a).FirstOrDefault();

            try
            {
                if (geraet != null)
                {
                    ctx.M13S0402.Remove(geraet);
                }
                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        public List<Geraet> GetGeraete()
        {
            /*
            List<Geraet> query = (from x in ctx.M13S0402
                join stamm in ctx.Stammtabelle on "GG" + x.GGruppe equals stamm.argument
                select new Geraet
                {
                    IdentNr = x.IdentNr,
                    charge = x.charge,
                    gstatus = x.gstatus,
                    baujahr = x.baujahr,
                    kunde = x.kunde,
                    gertyp = x.GerTYp,
                    inaktiv = x.inaktiv,
                    ggruppe = x.GGruppe,
                    Bezeichnung = stamm.bezeichnung
                }).ToList();
            */

            List<Geraet> geraete = ctx.M13S0402.OrderBy(x => x.IdentNr).AsEnumerable().Select(x => Mapper.Map<M13S0402, Geraet>(x)).ToList();
            return geraete;
        }


        public bool UpdateGeraet(M13S0402 geraet)
        {
            try
            {
                M13S0402 item = (from k in ctx.M13S0402
                                 where k.IdentNr == geraet.IdentNr
                    select k).FirstOrDefault();

                if (item != null)
                {
                    item = Mapper.Map<M13S0402>(geraet);
                    ctx.M13S0402.AddOrUpdate(item);

                    ctx.SaveChanges();

                    return true;
                }
                else
                {
                    // TODO log exception
                    throw new Exception("Kunde nicht gefunden.");
                }

            }
            catch (Exception ex)
            {
                // TODO log exception
                throw ex;
            }
        }


        public List<Geraet> GetGebrauchtGeraete()
        {
            /*
            List<Geraet> query = (from x in ctx.M13S0402
                join stamm in ctx.Stammtabelle on "GG" + x.GGruppe equals stamm.argument
                where x.gstatus == "25"
                select new Geraet
                {
                    IdentNr = x.IdentNr,
                    charge = x.charge,
                    gstatus = x.gstatus,
                    baujahr = x.baujahr,
                    kunde = x.kunde,
                    gertyp = x.GerTYp,
                    inaktiv = x.inaktiv,
                    ggruppe = x.GGruppe,
                    Bezeichnung = stamm.bezeichnung
                }).ToList();

            return query;
            */

            List<Geraet> geraete = ctx.M13S0402.Where(x => x.gstatus == "25").OrderBy(x => x.IdentNr).AsEnumerable().Select(x => Mapper.Map<M13S0402, Geraet>(x)).ToList();
            return geraete;
        }
    }
}
