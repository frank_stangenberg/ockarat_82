﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer.Stamm
{
    public class Stamm : IStamm
    {
        public string StammId { get; set; }
        public string Bezeichnung { get; set; }
    }
}
