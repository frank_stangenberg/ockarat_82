﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.Native;

namespace OCDatalayer.Stamm
{
    public class StammStammdaten : IStammStammdaten
    {
        OCKaratEntities ctx = new OCKaratEntities();


        public List<Stamm> GetStammListe()
        {
            List<Stamm> stammListe = ctx.Stammtabelle.OrderBy(x => x.bezeichnung).Select(x => new Stamm
            {
                StammId = x.argument,
                Bezeichnung = x.bezeichnung,
            }).ToList();


            return stammListe;
        }


        public List<Stamm> GetStammListeByPrefix(string prefix)
        {
            List<Stamm> stammListe = ctx.Stammtabelle.Where(x=>x.argument.StartsWith(prefix) ).OrderBy(x => x.bezeichnung).Select(x => new Stamm
            {
                StammId = x.argument.Substring(2),
                Bezeichnung = x.bezeichnung,
            }).ToList();

            return stammListe;
        }
    }
}
