﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OCDatalayer.Stamm
{
    public interface IStammStammdaten
    {
        List<Stamm> GetStammListe();

        List<Stamm> GetStammListeByPrefix(string prefix);
    }
}
