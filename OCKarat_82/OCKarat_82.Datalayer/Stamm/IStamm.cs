﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer.Stamm
{
    public interface IStamm
    {
        string StammId { get; set; }
        string Bezeichnung { get; set; }

    }
}
