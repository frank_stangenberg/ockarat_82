﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office.Utils;

namespace OCDatalayer.Stamm
{
    public class OCListen
    {
        public static readonly List<Stamm> AbcArtikel;
        public static readonly List<Stamm> ArtikelKennzeichen;
        public static readonly List<Stamm> GeraeteGruppen;
        public static readonly List<Stamm> GeraetStatus;
        public static readonly List<Stamm> Hersteller;
        public static readonly List<Stamm> MengenEinheiten;
        public static readonly List<Stamm> PreisEinheiten;
        public static readonly List<Stamm> KundenKennzeichen;
        public static readonly List<Stamm> RabattGruppen;
        public static readonly List<Stamm> Tonnagen;
        public static readonly List<Stamm> VerpackungsEinheiten;
        public static readonly List<Stamm> WarenGruppen;
        public static readonly List<Stamm> Verbrauchsschluessel;
        public static readonly List<Stamm> Zahlungsbedingungen;

        static OCListen()
        {
            var stammDaten = new StammStammdaten();
            AbcArtikel = stammDaten.GetStammListeByPrefix("AA");
            ArtikelKennzeichen = stammDaten.GetStammListeByPrefix("PA");
            GeraeteGruppen = stammDaten.GetStammListeByPrefix("GG");
            GeraetStatus = stammDaten.GetStammListeByPrefix("GT");
            Hersteller = stammDaten.GetStammListeByPrefix("HC");
            MengenEinheiten = stammDaten.GetStammListeByPrefix("ME");
            PreisEinheiten = stammDaten.GetStammListeByPrefix("PE");
            RabattGruppen = stammDaten.GetStammListeByPrefix("RG");
            Tonnagen = stammDaten.GetStammListeByPrefix("GA");
            VerpackungsEinheiten = stammDaten.GetStammListeByPrefix("VE");
            WarenGruppen = stammDaten.GetStammListeByPrefix("WG");
            Verbrauchsschluessel = new List<Stamm>
            {
                new Stamm {StammId = "V", Bezeichnung = "Verbrauch"},
                new Stamm {StammId = "B", Bezeichnung = "Bedarf"}
            };
            Zahlungsbedingungen = stammDaten.GetStammListeByPrefix("34");
        }
    }
}
