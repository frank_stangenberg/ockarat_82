﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public interface IKontaktperson
    {
        string Id { get; set; }
        string Anrede { get; set; }
        string Name { get; set; }
        string Vorname { get; set; }
        string Titel { get; set; }
        string N { get; set; }
        string A { get; set; }
        string SG { get; set; }
        string Abteilung { get; set; }
        string Funktion { get; set; }
        string Marketingabteilung { get; set; }
        string Firmenabteilung { get; set; }
        string Zustaendigfuer { get; set; }
        string Geburtsdatum { get; set; }
        //Firmen Daten
        string EMail { get; set; }
        string ArbeitsplatzTel { get; set; }
        string ArbeitsplatzFax { get; set; }
        string Handy { get; set; }
        //Private Daten
        string Ort { get; set; }
        string Strasse { get; set; }
        string Plz { get; set; }
        string Telefon { get; set; }
        string Telefax { get; set; }
        string Internet { get; set; }

        string Notizen { get; set; }

    }
}
