﻿using OCDatalayer.Stamm;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using AutoMapper;

namespace OCDatalayer
{
    public class KundenStammdaten : IKundenStammdaten
    {
        OCKaratEntities ctx = new OCKaratEntities();

        public bool CreateKunde(Kunde kunde)
        {
            try
            {
                A13S0101 toInsert = Mapper.Map<A13S0101>(kunde);
                ctx.A13S0101.Add(toInsert);

                ctx.SaveChanges();

                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public bool DeleteKundeById(string KundeID)
        {
            var item = (from k in ctx.A13S0101
                        where k.KUNDE == KundeID
                        select k).FirstOrDefault();

            try
            {
                if (item != null)
                {
                    ctx.A13S0101.Remove(item);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        /// <summary>
        /// holt den Kunden zu einer gegebenen ID
        /// </summary>
        /// <param name="KundeID"></param>
        /// <returns></returns>
        public Kunde GetKundeById(string KundeID)
        {
            var item = (from k in ctx.A13S0101
                         where k.KUNDE == KundeID
                         select k).FirstOrDefault();

            Kunde kunde = new Kunde();
            kunde = Mapper.Map(item, kunde);

            return kunde;
        }

        /// <summary>
        /// holt den vollständigen Datensatz des Kunden abhängig von der KundenId
        /// </summary>
        /// <param name="kundeId">
        /// id des Kunden der angezeigt werden soll
        /// </param>
        /// <returns>volständigen Kundendatensatz</returns>
        public A13S0101 GetFullKundeById(string kundeId)
        {
            var kunde = (from k in ctx.A13S0101
                where k.KUNDE == kundeId
                select k).FirstOrDefault();

            return kunde;
        }

        /// <summary>
        /// holt die Liste der Kunden, aufbereitet für die Anzeige in der Kundenübersicht
        /// </summary>
        /// <param name="suchString"></param>
        /// <returns></returns>
        public List<Kunde> GetKunden(string suchString = null)
        {
            List<Kunde> kunden = ctx.A13S0101.Where(x => x.Kd_Kennz == "K").Where(s => s.Name1.Contains(suchString) || s.Name2.Contains(suchString) || s.Name3.Contains(suchString)).OrderBy(x => x.Name1).Select(x => Mapper.Map<A13S0101, Kunde>(x)).ToList();

            return kunden;
        }

        /// <summary>
        /// holt die List der Kunden, abhängig von den gegebenen Suchparametern
        /// </summary>
        /// <param name="KundeId"></param>
        /// <param name="name1"></param>
        /// <param name="name2"></param>
        /// <param name="name3"></param>
        /// <param name="strasse"></param>
        /// <param name="plz"></param>
        /// <param name="ort"></param>
        /// <returns></returns>
        public List<Kunde> GetKunden(string KundeId = null, string name1 = null, string name2 = null, string name3 = null, string strasse = null, string plz = null, string ort = null)
        {
            var abfrage = ctx.A13S0101.Where(x => x.Kd_Kennz == "K");
            if (!string.IsNullOrEmpty(KundeId))
            {
                abfrage = abfrage.Where(s => s.KUNDE == KundeId);
            }
            if (!string.IsNullOrEmpty(name1))
            {
                abfrage = abfrage.Where(s => s.Name1.Contains(name1));
            }
            if (!string.IsNullOrEmpty(name2))
            {
                abfrage = abfrage.Where(s => s.Name2.Contains(name2));
            }
            if (!string.IsNullOrEmpty(name3))
            {
                abfrage = abfrage.Where(s => s.Name3.Contains(name3));
            }
            if (!string.IsNullOrEmpty(strasse))
            {
                abfrage = abfrage.Where(s => s.STRASSE.Contains(strasse));
            }
            if (!string.IsNullOrEmpty(plz))
            {
                abfrage = abfrage.Where(s => s.PLZ.Contains(plz));
            }
            if (!string.IsNullOrEmpty(ort))
            {
                abfrage = abfrage.Where(s => s.ORT.Contains(ort));
            }

            List<Kunde> kunden = abfrage.OrderBy(s => s.Name1).AsEnumerable().Select(x => Mapper.Map<A13S0101, Kunde>(x)).ToList();


            return kunden;
        }

        /// <summary>
        /// holt die Kunden abhängig vom ihrem Kennzeichen (L = Lieferant, I = Interessent, ...)
        /// </summary>
        /// <param name="kennzeichen"></param>
        /// <returns>Liste der Kunden mit diesem Kennzeichen</returns>
        public List<Kunde> GetKundenByKennzeichen(string kennzeichen)
        {
            //List<Kunde> kunden = ctx.A13S0101.Where(x=>x.Kd_Kennz == kennzeichen).OrderBy(x => x.Name1).Select(x => Mapper.Map<A13S0101, Kunde>(x)).ToList();
            List<Kunde> kunden = ctx.A13S0101.Where(x => x.Kd_Kennz == "K").OrderBy(x => x.Name1).AsEnumerable().Select(x => Mapper.Map<A13S0101, Kunde>(x)).ToList();
            return kunden;
        }


        /// <summary>
        /// aktualisiert einen bearbeiteten Kunden
        /// </summary>
        /// <param name="kunde"></param>
        /// <returns>true = success, false = Fehler</returns>
        public bool UpdateKunde(Kunde kunde)
        {
            try
            {
                A13S0101 item = (from k in ctx.A13S0101
                    where k.KUNDE == kunde.KUNDE
                    select k).FirstOrDefault();

                if (item != null)
                {
                    item = Mapper.Map<A13S0101>(kunde);
                    ctx.A13S0101.AddOrUpdate(item);

                    ctx.SaveChanges();

                    return true;
                }
                else
                {
                    // TODO log exception
                    throw new Exception("Kunde nicht gefunden.");
                }

            }
            catch (Exception ex)
            {
                // TODO log exception
                throw ex;
            }
        }

    }
}
