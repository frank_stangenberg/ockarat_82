﻿using System;
using System.Collections.Generic;

namespace OCDatalayer
{
    public class Kunde : IKunde
    {
        public string KUNDE { get; set; }
        public string STATUS { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string match { get; set; }
        public string STRASSE { get; set; }
        public string PLZ { get; set; }
        public string ORT { get; set; }
        public string PLZPF { get; set; }
        public string POSTFACH { get; set; }
        public string TELEFON { get; set; }
        public string AUTOTELEFON { get; set; }
        public string TELEFAX { get; set; }
        public string Internet { get; set; }
        public string E_Mail { get; set; }
        public string Kd_Kennz { get; set; }
        public string Zahlbed { get; set; }
        public string VERSANDBED { get; set; }
        public bool? LIEFERSPERRE { get; set; }
        public string VERTRETER { get; set; }
        public decimal? KREDITLIMIT { get; set; }
        public string DATNEU { get; set; }
        public string DATAEND { get; set; }
        public string PNRNEU { get; set; }
        public string PNRAEND { get; set; }
        public string REGION { get; set; }
        public string mwst { get; set; }
        public string BESUCH { get; set; }
        public string Land { get; set; }
        public string Sprache { get; set; }
        public string Branche { get; set; }
        public string Waehrung { get; set; }
        public string Preisliste { get; set; }
        public bool? St_Rabatt { get; set; }
        public bool? DrOrgNr { get; set; }
        public bool? Mahnsperr { get; set; }
        public decimal? KredLimit { get; set; }
        public decimal? KredLimit2 { get; set; }
        public decimal? Rabatt { get; set; }
        public decimal? Vorfracht { get; set; }
        public decimal? T_Rabatt { get; set; }
        public decimal? W_Rabatt { get; set; }
        public decimal? M_Rabatt { get; set; }
        public string RechnAn { get; set; }
        public string FBank_nr { get; set; }
        public string KopRechn { get; set; }
        public string KopAngeb { get; set; }
        public string KopAB { get; set; }
        public string KopRapp { get; set; }
        public string KopLief { get; set; }
        public string KopBest { get; set; }
        public string offenes_Angebot { get; set; }
        public string UST_ID { get; set; }
        public string KuLiNr { get; set; }
        public string HCode { get; set; }
        public string Oeffnungszeiten { get; set; }
        public string Tour { get; set; }
        public string Kundenklasse { get; set; }
        public decimal? Mietrabatt { get; set; }
        public bool? keine_Werbung { get; set; }
        public bool? mit_AW { get; set; }
        public string Erlösgruppe { get; set; }
        public string Versandart { get; set; }
        public string Jahresbestnr { get; set; }
        public string Name3 { get; set; }
        public DateTime? letzter_Kontakt { get; set; }
        public DateTime? letzte_Anfrage { get; set; }
        public string letzter_Kontakt_Pnr { get; set; }
        public string letzte_Anfrage_Pnr { get; set; }
        public bool? hst_preis { get; set; }
        public string L_Schein { get; set; }
        public string Rechnung { get; set; }
        public string S_Rechnung { get; set; }
        public string Konzern { get; set; }
        public string Kustnr { get; set; }
        public string A_Auftrag { get; set; }
        public bool? WerbeKunde { get; set; }
        public string fibu_kunde { get; set; }
        public string Kunde_alt { get; set; }
        public string info { get; set; }
        public bool? vorkasse { get; set; }
        public string HaendlerNr { get; set; }
        public bool? Ebestellung { get; set; }
        public int? anzahl_geräte { get; set; }
        public bool? Outlook { get; set; }
        public bool? skto_eteile { get; set; }
        public bool? netto_eingabe { get; set; }
        public bool? Projektnrverwenden { get; set; }
        public string PF_Ort { get; set; }
        public string KZ_Uebergrenz { get; set; }
        public bool? chk_bankeinzug { get; set; }
        public byte? Mandant { get; set; }
        public string Steuerschlüssel { get; set; }
        public bool? ek_rundung { get; set; }
        public string TAPI_TelNr { get; set; }
        public string Monteur { get; set; }
        public string TAPI_Handy { get; set; }
        public string ABC_Miete { get; set; }
        public string ABC_Service { get; set; }
        public string ABC_Verkauf { get; set; }
        public string INFO_Miete { get; set; }
        public string INFO_Verkauf { get; set; }
        public string INFO_Service { get; set; }
        public DateTime? letzter_Besuch { get; set; }
        public DateTime? letzte_Besprechung { get; set; }
        public string Rechtsform { get; set; }
        public string bon_umsatz_jahr { get; set; }
        public string bon_auskunft_von { get; set; }
        public string bon_schriftlich { get; set; }
        public string bon_vermerke { get; set; }
        public string bon_zahlungserfahrung { get; set; }
        public decimal? vorfrachtek { get; set; }
        public bool? geprüft { get; set; }
        public string Textbaustein { get; set; }
        public string bon_index { get; set; }
        public string bon_eingeholt { get; set; }
        public string info2 { get; set; }
        public bool? eb_linde { get; set; }
        public bool? eb_kaercher { get; set; }
        public bool? eb_NexMart { get; set; }
        public bool? inaktiv { get; set; }
        public string s_nachweis1 { get; set; }
        public string s_nachweis2 { get; set; }
        public bool? kz_divers { get; set; }
        public string abteilung { get; set; }
        public decimal? auftragsrabatt { get; set; }
        public string suchfeld { get; set; }
        public string hersteller_von { get; set; }
        public bool? eb_kärcher { get; set; }
        public bool? eb_kubota { get; set; }
        public bool? chk_hstamm_von_brutto { get; set; }
        public bool? chk_update { get; set; }
        public bool? chk_kein_feser { get; set; }
        public string anrede { get; set; }
        public bool? keine_onlinewerbung { get; set; }
        public string infofeld1 { get; set; }
        public string infofeld2 { get; set; }
        public string betriebsgrösse { get; set; }
        public string anzahl_besuche { get; set; }
        public string anzahl_telefonate { get; set; }
        public string letzter_kontakt_vertrieb_pnr { get; set; }
        public DateTime? letzter_kontakt_vertrieb { get; set; }
        public bool? eb_tvh { get; set; }
        public string fahrzone { get; set; }
        public bool? eb_yale { get; set; }
        public bool? chk_keine_margenprüfung { get; set; }
        public string email_faktura { get; set; }
        public bool? chk_emailversand_faktura { get; set; }
        public bool? chk_emailversand_allgemein { get; set; }
        public string shop_anbieter { get; set; }
        public bool? chk_shop_käufer { get; set; }
        public bool? chk_emailversand { get; set; }
        public string letzter_kontakt_jmt { get; set; }
        public bool? chk_gefunden { get; set; }
        public bool? chk_mandant1 { get; set; }
        public bool? chk_mandant2 { get; set; }
        public string bundesland { get; set; }
        public string blz1 { get; set; }
        public string bank1 { get; set; }
        public string ktonr1 { get; set; }
        public string blz2 { get; set; }
        public string bank2 { get; set; }
        public string ktonr2 { get; set; }
        public string blz3 { get; set; }
        public string bank3 { get; set; }
        public string ktonr3 { get; set; }

        public List<Kontaktperson> Kontaktpersonen { get; set; }
    }
}
