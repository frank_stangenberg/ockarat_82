﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer
{
    public class Kontaktperson : IKontaktperson
    {
        public string Id { get; set; }
        public string Anrede { get; set; }
        public string Name { get; set; }
        public string Vorname { get; set; }
        public string Titel { get; set; }
        public string N { get; set; }
        public string A { get; set; }
        public string SG { get; set; }
        public string Abteilung { get; set; }
        public string Funktion { get; set; }
        public string Marketingabteilung { get; set; }
        public string Firmenabteilung { get; set; }
        public string Zustaendigfuer { get; set; }
        public string Geburtsdatum { get; set; }
        //Firmen Daten
        public string EMail { get; set; }
        public string ArbeitsplatzTel { get; set; }
        public string ArbeitsplatzFax { get; set; }
        public string Handy { get; set; }
        //Private Daten
        public string Ort { get; set; }
        public string Strasse { get; set; }
        public string Plz { get; set; }
        public string Telefon { get; set; }
        public string Telefax { get; set; }
        public string Internet { get; set; }

        public string Notizen { get; set; }

    }
}
