﻿using System.Collections.Generic;

namespace OCDatalayer
{
    public interface IKundenStammdaten
    {
        List<Kunde> GetKunden(string KundeId = null, string name1 = null, string name2 = null, string name3 = null, string strasse = null, string plz = null, string ort = null);

        List<Kunde> GetKundenByKennzeichen(string kennzeichen);

        Kunde GetKundeById(string kundeID);

        bool CreateKunde(Kunde kunde);

        bool UpdateKunde(Kunde kunde);

        bool DeleteKundeById(string kundeID);

    }
}
