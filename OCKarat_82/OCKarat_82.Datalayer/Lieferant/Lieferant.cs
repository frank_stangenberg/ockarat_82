﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer.Lieferant
{
    public class Lieferant : ILieferant
    {
        public string LieferantenId { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
    }
}
