﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer.Lieferant
{
    public interface ILieferant
    {
        string LieferantenId { get; set; }
        string Status { get; set; }
        string Name { get; set; }
    }
}
