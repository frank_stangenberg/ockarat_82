﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OCDatalayer.Lieferant
{
    public interface ILieferantenStammdaten
    {
        List<Lieferant> GetLieferanten();

        Lieferant GetLieferantById(string lieferantID);

        bool CreateLieferant(Lieferant lieferant);

        Lieferant UpdateLieferant(Lieferant lieferant);

        bool DeleteLieferantById(string lieferantID);
    }
}
