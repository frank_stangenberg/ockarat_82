﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCDatalayer.Lieferant
{
    public class LieferantenStammdaten : ILieferantenStammdaten
    {
        OCKaratEntities ctx = new OCKaratEntities();

        public bool CreateLieferant(Lieferant lieferant)
        {
            // TODO design Lieferanten-Tabelle
            var item = new A13S0101();

            try
            {
                item.KUNDE = lieferant.LieferantenId;
                item.Name1 = lieferant.Name;
                item.STATUS = lieferant.Status;

                //TODO change to Lieferanten-Tabelle
                ctx.A13S0101.Add(item);
                ctx.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        public bool DeleteLieferantById(string LieferantenID)
        {
            //TODO change to Lieferanten-Tabelle
            var item = (from l in ctx.A13S0101
                        where l.KUNDE == LieferantenID
                        select l).FirstOrDefault();

            try
            {
                if (item != null)
                {
                    //TODO change to Lieferanten-Tabelle
                    ctx.A13S0101.Remove(item);
                }
                return true;
            }
            catch (Exception)
            {
                // TODO log exception
                return false;
            }
        }

        public Lieferant GetLieferantById(string LieferantenID)
        {
            //TODO change to Lieferanten-Tabelle
            var item = (from l in ctx.A13S0101
                         where l.KUNDE == LieferantenID
                         select l).FirstOrDefault();

            Lieferant lieferant = new Lieferant();
            lieferant.LieferantenId = item.KUNDE;
            lieferant.Status = item.STATUS;
            lieferant.Name = item.Name1;

            return lieferant;
        }

        public List<Lieferant> GetLieferanten()
        {
            List<Lieferant> lieferanten = new List<Lieferant>();

            //TODO change to Lieferanten-Tabelle
            foreach (var item in ctx.A13S0101)
            {
                Lieferant lieferant = new Lieferant();
                lieferant.LieferantenId= item.KUNDE;
                lieferant.Status = item.STATUS;
                lieferant.Name = item.Name1;

                lieferanten.Add(lieferant);
            }

            return lieferanten;
        }


        public Lieferant UpdateLieferant(Lieferant lieferant)
        {

            //TODO change to Lieferanten-Tabelle
            var item = (from l in ctx.A13S0101
                        where l.KUNDE == lieferant.LieferantenId
                        select l).FirstOrDefault();

            try
            {
                if (item != null)
                {
                    item.STATUS = lieferant.Status;
                    item.Name1 = lieferant.Name;

                    ctx.SaveChanges();

                    return lieferant;
                }
                else
                {
                    // TODO log exception
                    throw new Exception("User not found");
                }

            }
            catch (Exception ex)
            {
                // TODO log exception
                throw ex;
            }
        }
    }
}
