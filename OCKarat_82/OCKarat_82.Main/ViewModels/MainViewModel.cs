﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace OCKarat_82.Main.ViewModels
{
    public class MainViewModel
    {
        public static MainViewModel Create()
        {
            return ViewModelSource.Create(() => new MainViewModel());
        }
    }
}
