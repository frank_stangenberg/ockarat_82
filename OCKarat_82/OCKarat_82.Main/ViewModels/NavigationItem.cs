﻿using DevExpress.Mvvm;
using OCKarat_82.Common;
using System;

namespace OCKarat_82.Main.ViewModels
{
    [Serializable]
    public class NavigationItem : INavigationItem
    {
        public string Caption { get; set; }
        public string GroupName { get; set; }

        public NavigationItem() { }

        public NavigationItem(string caption, string groupName)
        {
            Caption = caption;
            GroupName = groupName;
        }
    }
}
