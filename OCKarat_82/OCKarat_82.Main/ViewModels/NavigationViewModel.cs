﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCKarat_82.Modules.ViewModels;
using OCKarat_82.Modules.Views;

namespace OCKarat_82.Main.ViewModels
{
    public static class NavigationViewModel
    {

        public static List<ExtendedNavigationItem> GetNavigationItems()
        {
            var navigationItems = new List<ExtendedNavigationItem>
            {
                // Stammdaten
                new ExtendedNavigationItem {Caption = "Kundenstamm", GroupName = "Stammdaten", Key = "StammKundenView", UseItem = true, ModelFactory = () => StammKundenViewModel.Create(), ViewType = typeof(StammKundenView)},
                new ExtendedNavigationItem {Caption = "Interessenten", GroupName = "Stammdaten", Key = "StammInteressentenView", UseItem = true, ModelFactory = () => StammInteressentenViewModel.Create(), ViewType = typeof(StammInteressentenView)},
                new ExtendedNavigationItem {Caption = "Lieferanten", GroupName = "Stammdaten", Key = "StammLieferantenView", UseItem = true, ModelFactory = () => StammLieferantenViewModel.Create(), ViewType = typeof(StammLieferantenView)},
                new ExtendedNavigationItem {Caption = "Artikel", GroupName = "Stammdaten", Key = "StammArtikelView", UseItem = true, ModelFactory = () => StammArtikelViewModel.Create(), ViewType = typeof(StammArtikelView)},
                //new ExtendedNavigationItem {Caption = "Modelle", GroupName = "Stammdaten", Key = "StammModelleView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Geräte", GroupName = "Stammdaten", Key = "StammGeraeteView", UseItem = true, ModelFactory = () => StammGeraeteViewModel.Create(), ViewType = typeof(StammGeraeteView)},
                new ExtendedNavigationItem {Caption = "Gebrauchtgeräte", GroupName = "Stammdaten", Key = "StammGebrauchtGeraeteView", UseItem = true, ModelFactory = () => StammGebrauchtGeraeteViewModel.Create(), ViewType = typeof(StammGebrauchtGeraeteView)},
                new ExtendedNavigationItem {Caption = "Ersatzteile/Zubehör", GroupName = "Stammdaten", Key = "StammErsatzteileView", UseItem = true, ModelFactory = () => StammErsatzteileViewModel.Create(), ViewType = typeof(StammErsatzteileView)},
                //new ExtendedNavigationItem {Caption = "Mitarbeiter", GroupName = "Stammdaten", Key = "StammMitarbeiterView", UseItem = false},
                //new ExtendedNavigationItem {Caption = "Hersteller", GroupName = "Stammdaten", Key = "StammHerstellerView", UseItem = false},

                // Verkauf
                new ExtendedNavigationItem {Caption = "Angebot/Kostenvoranschlag", GroupName = "Verkauf", Key = "VerkaufAngebotView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Wiedervorlage", GroupName = "Verkauf", Key = "VerkaufWiedervorlageView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Auftragsbearbeitung(Erfassung, Bestätigung)", GroupName = "Verkauf", Key = "VerkaufAuftragView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Rechnung", GroupName = "Verkauf", Key = "VerkaufRechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Sammelrechnung", GroupName = "Verkauf", Key = "VerkaufSammelrechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Lieferschein", GroupName = "Verkauf", Key = "VerkaufLieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Gerätelieferschein", GroupName = "Verkauf", Key = "VerkaufGeraetelieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Transportlieferschein", GroupName = "Verkauf", Key = "VerkaufTransportlieferscheinView", UseItem = false},

                // Miete
                new ExtendedNavigationItem {Caption = "Mietauftragsangebot", GroupName = "Miete", Key = "MieteAngebotView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Wiedervorlage", GroupName = "Miete", Key = "MieteWiedervorlagevView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Mietauftragsbearbeitung", GroupName = "Miete", Key = "MieteAuftragView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Mietvertrag", GroupName = "Miete", Key = "MieteVertragView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Leasingvertrag", GroupName = "Miete", Key = "MieteLeasingView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Fullservicevertrag", GroupName = "Miete", Key = "MieteFullserviceView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Rechnung(Miete, Leasing, Fullservice)", GroupName = "Miete", Key = "MieteRechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Sammelrechnung", GroupName = "Miete", Key = "MieteSammelrechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Mietlieferschein", GroupName = "Miete", Key = "MieteLieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Abhollieferschein", GroupName = "Miete", Key = "MieteAbhollieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Gerätelieferschein", GroupName = "Miete", Key = "MieteGeraetlieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Transportlieferschein", GroupName = "Miete", Key = "MieteTransportlieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Mietdisposition", GroupName = "Miete", Key = "MieteDispositionView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Analyse Vermietung", GroupName = "Miete", Key = "MieteAnalyseView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Mietkalkulation", GroupName = "Miete", Key = "MieteKalkulationView", UseItem = false},

                // Service
                new ExtendedNavigationItem {Caption = "Serviceauftrag", GroupName = "Service", Key = "ServiceAuftragView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Fullserviceauftrag", GroupName = "Service", Key = "ServiceFullserviceView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Kostenvoranschlag", GroupName = "Service", Key = "ServiceKostenvoranschlagView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Auftragsbestätigung", GroupName = "Service", Key = "ServiceAuftragsbestaetigungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Lieferschein", GroupName = "Service", Key = "ServiceLieferscheinView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Rechnung", GroupName = "Service", Key = "ServiceRechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "UVV / FEM Vertrag", GroupName = "Service", Key = "ServiceUVVFEMView", UseItem = false},
                new ExtendedNavigationItem {Caption = "ASU Vertrag", GroupName = "Service", Key = "ServiceASUView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Garantieabwicklung", GroupName = "Service", Key = "ServiceGarantieView", UseItem = false},

                // Einkauf
                new ExtendedNavigationItem {Caption = "Bestellung", GroupName = "Einkauf", Key = "EinkaufBestellungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Bestellbearbeitung", GroupName = "Einkauf", Key = "EinkaufBestellbearbeitungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Rechnungsprüfung", GroupName = "Einkauf", Key = "EinkaufRechnungspruefungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Wareneingang", GroupName = "Einkauf", Key = "EinkaufWareneingangView", UseItem = false},

                // Lager
                new ExtendedNavigationItem {Caption = "Lagerbestandsführung", GroupName = "Lager", Key = "LagerBestandsfuerhungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Inventurbearbeitung", GroupName = "Lager", Key = "LagerInventurView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Zugangsliste", GroupName = "Lager", Key = "LagerZugangslisteView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Abgangsliste", GroupName = "Lager", Key = "LagerAbgangslisteView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Lagerbestandsliste", GroupName = "Lager", Key = "LagerBestandslisteView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Lagerwertliste", GroupName = "Lager", Key = "LagerWertelisteView", UseItem = false},

                // Statistik
                new ExtendedNavigationItem {Caption = "Neue Rechnung", GroupName = "Statistik", Key = "StatistikNeueRechnungView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Offene Angebote", GroupName = "Statistik", Key = "StatistikOffeneAngeboteView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Auftragseingang", GroupName = "Statistik", Key = "StatistikAuftragseingangView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Kundenstatistik", GroupName = "Statistik", Key = "StatistikKundenView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Vertreterstatistik", GroupName = "Statistik", Key = "StatistikVertreterView", UseItem = false},

                // Einstellungen
                new ExtendedNavigationItem {Caption = "Schnittstellen -/ Synchronisationsmanagement", GroupName = "Einstellungen", Key = "EinstellungenSchnittstellenView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Stammtabellenverwaltung", GroupName = "Einstellungen", Key = "EinstellungenStammtabelleView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Textbausteine", GroupName = "Einstellungen", Key = "EinstellungenTextbausteineView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Textkonserven", GroupName = "Einstellungen", Key = "EinstellungenTextkonservenView", UseItem = false},
                new ExtendedNavigationItem {Caption = "Datenbank", GroupName = "Einstellungen", Key = "EinstellungenDatenbankView", UseItem = false}
            };
            return navigationItems;
        }
    }
}
