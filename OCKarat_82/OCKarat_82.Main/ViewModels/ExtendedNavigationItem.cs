﻿using DevExpress.Mvvm;
using OCKarat_82.Common;
using System;

namespace OCKarat_82.Main.ViewModels
{
    [Serializable]
    public class ExtendedNavigationItem : IExtendedNavigationItem
    {
        public string Caption { get; set; }
        public string GroupName { get; set; }
        public string Key { get; set; }
        public bool UseItem { get; set; }
        public Func<object> ModelFactory { get; set; }
        public Type ViewType { get; set; }

        public ExtendedNavigationItem() { }

        public ExtendedNavigationItem(string caption, string groupName, string key, bool useItem)
        {
            Caption = caption;
            GroupName = groupName;
            Key = key;
            UseItem = useItem;
        }

        public ExtendedNavigationItem(string caption, string groupName, string key, bool useItem, Func<object> modelFactory, Type viewType)
        {
            Caption = caption;
            GroupName = groupName;
            Key = key;
            UseItem = useItem;
            ModelFactory = modelFactory;
            ViewType = viewType;
        }
    }
}
