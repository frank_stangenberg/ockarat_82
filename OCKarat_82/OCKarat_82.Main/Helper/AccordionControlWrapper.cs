﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Mvvm.UI.ModuleInjection;
using DevExpress.Xpf.Accordion;
using OCKarat_82.Main.ViewModels;

namespace OCKarat_82.Main
{
    public class AccordionControlWrapper : ISelectorWrapper<AccordionControl>
    {
        private object _itemsSource;

        public object ItemsSource
        {
            get => _itemsSource;
            set
            {
                if (_itemsSource != value)
                {
                    if (_itemsSource != null)
                        ((INotifyCollectionChanged) _itemsSource).CollectionChanged -=
                            AccordionControlWrapper_CollectionChanged;

                    _itemsSource = value;
                    ((INotifyCollectionChanged) _itemsSource).CollectionChanged +=
                        AccordionControlWrapper_CollectionChanged;
                }
            }
        }

        public DataTemplate ItemTemplate { get; set; }
        public DataTemplateSelector ItemTemplateSelector { get; set; }
        public AccordionControl Target { get; set; }
        public object SelectedItem { get { return Target.SelectedItem; } set { Target.SelectedItem = value; } }

        public event EventHandler SelectionChanged {
            add { Target.SelectedItemChanged += new EventHandler<AccordionSelectedItemChangedEventArgs>(value); }
            remove { Target.SelectedItemChanged -= new EventHandler<AccordionSelectedItemChangedEventArgs>(value); }
        }

        private void AccordionControlWrapper_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (NavigationItem newItem in e.NewItems)
                {
                    // Search if a group with the name already exists in the AccordionControl.
                    var group = Target.Items.FirstOrDefault(x => ((AccordionItem) x).Header.Equals(newItem.GroupName));
                    // If group does not yet exist, create new group.
                    if (group == null && newItem.GroupName != null)
                    {
                        group = new AccordionItem();
                        ((AccordionItem) group).Header = newItem.GroupName;
                        Target.Items.Add(group);
                    }

                    // Creating a New Instance of the Navigation Element.
                    var accordionItem = new AccordionItem {
                        Header = newItem.Caption,
                        Tag = newItem
                    };
                    // Add navigation element to the group. If no group exists, insert it directly.
                    if (group != null)
                        ((AccordionItem) group).Items.Add(accordionItem);
                    else
                        Target.Items.Add(accordionItem);
                }
        }
    }

    public class AccordionControlStrategy : ItemsControlStrategy<AccordionControl, AccordionControlWrapper> {
        protected override void InitializeCore() {
            base.InitializeCore();
            Wrapper.SelectionChanged += Wrapper_SelectionChanged;
        }

        protected override void UninitializeCore() {
            Wrapper.SelectionChanged -= Wrapper_SelectionChanged;
            base.UninitializeCore();
        }

        private void Wrapper_SelectionChanged(object sender, EventArgs e) {
            var item = (AccordionItem)Wrapper.SelectedItem;
            SelectedViewModel = item.Tag;
        }

        protected override void OnSelectedViewModelChanged(object oldValue, object newValue) {
            base.OnSelectedViewModelChanged(oldValue, newValue);
            var item = Find(newValue as NavigationItem, Wrapper.Target.Items);
            Wrapper.SelectedItem = item;
        }

        private object Find(NavigationItem newValue, ObservableCollection<object> items) {
            foreach (AccordionItem item in items) {
                if (item.Tag == newValue)
                    return item;
                var result = Find(newValue, item.Items);
                if (result != null)
                    return result;
            }
            return null;
        }
    }
}