﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Core;
using OCKarat_82.Common;
using OCKarat_82.Main.Properties;
using OCKarat_82.Main.ViewModels;
using OCKarat_82.Main.Views;
using OCKarat_82.Modules.ViewModels;
using OCKarat_82.Modules.Views;
using System.ComponentModel;
using System.Windows;
using DevExpress.Mvvm.UI.ModuleInjection;
using DevExpress.Xpf.Accordion;
using OCDatalayer;
using AutoMapper;
using AppModules = OCKarat_82.Common.Modules;

namespace OCKarat_82.Main
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //ApplicationThemeHelper.UpdateApplicationThemeName();
            new Bootstrapper().Run();
        }
        protected override void OnExit(ExitEventArgs e)
        {
            //ApplicationThemeHelper.SaveApplicationThemeName();
            base.OnExit(e);
        }
    }
    public class Bootstrapper
    {
        const string StateVersion = "1.0";
        public virtual void Run()
        {
            // Registering own injection strategy for the AccordionControl
            StrategyManager.Default
                .RegisterStrategy<AccordionControl, AccordionControlStrategy>();

            ConfigureTypeLocators();
            RegisterModules();
            if (!RestoreState())
                InjectModules();
            ConfigureNavigation();
            ConfigureAutomapper();
            ShowMainWindow();
        }

        protected IModuleManager Manager { get { return ModuleManager.DefaultManager; } }
        protected virtual void ConfigureTypeLocators()
        {
            var mainAssembly = typeof(MainViewModel).Assembly;
            var modulesAssembly = typeof(StammArtikelDetailViewModel).Assembly;
            var assemblies = new[] { mainAssembly, modulesAssembly };
            ViewModelLocator.Default = new ViewModelLocator(assemblies);
            ViewLocator.Default = new ViewLocator(assemblies);
        }

        protected virtual void RegisterModules()
        {
            Manager.Register(Regions.MainWindow, new Module(AppModules.Main, MainViewModel.Create, typeof(MainView)));

            foreach (ExtendedNavigationItem item in NavigationViewModel.GetNavigationItems())
            {
                // use extendes navigation list for setting up navigation pane
                Manager.Register(Regions.Navigation, new Module(item.Key, () => new NavigationItem{Caption = item.Caption, GroupName = item.GroupName}));
            }

            // register modules
            foreach (ExtendedNavigationItem item in NavigationViewModel.GetNavigationItems())
            {
                // and if item is in use, register it
                if (item.UseItem)
                {
                    Manager.Register(Regions.Documents, new Module(item.Key, item.ModelFactory, item.ViewType));
                }
            }

        }


        protected virtual bool RestoreState()
        {
#if !DEBUG
            if (Settings.Default.StateVersion != StateVersion) return false;
            return Manager.Restore(Settings.Default.LogicalState, Settings.Default.VisualState);
#else
            return false;
#endif
        }

        protected virtual void InjectModules()
        {
            Manager.Inject(Regions.MainWindow, AppModules.Main);

            // inject navigation Item to navigation region
            foreach (ExtendedNavigationItem item in NavigationViewModel.GetNavigationItems())
            {
                Manager.Inject(Regions.Navigation, item.Key);

            }
        }

        protected virtual void ConfigureNavigation()
        {
            Manager.GetEvents(Regions.Navigation).Navigation += OnNavigation;
            Manager.GetEvents(Regions.Documents).Navigation += OnDocumentsNavigation;
        }

        /// <summary>
        /// Konfiguration für den Automapper
        /// </summary>
        private void ConfigureAutomapper()
        {
            Mapper.Initialize(cfg => {

                cfg.CreateMap<A13S0101, Kunde>().ForMember(k => k.Kontaktpersonen, i => i.Ignore());
            });
        }

        protected virtual void ShowMainWindow()
        {
            App.Current.MainWindow = new MainWindow();
            App.Current.MainWindow.Show();
            App.Current.MainWindow.Closing += OnClosing;
        }

        void OnNavigation(object sender, NavigationEventArgs e)
        {
            if (e.NewViewModelKey == null) return;
            Manager.InjectOrNavigate(Regions.Documents, e.NewViewModelKey);
        }

        void OnDocumentsNavigation(object sender, NavigationEventArgs e)
        {
            Manager.Navigate(Regions.Navigation, e.NewViewModelKey);
        }

        void OnClosing(object sender, CancelEventArgs e)
        {
            string logicalState;
            string visualState;
            /*
            Manager.Save(out logicalState, out visualState);
            Settings.Default.StateVersion = StateVersion;
            Settings.Default.LogicalState = logicalState;
            Settings.Default.VisualState = visualState;
            Settings.Default.Save();
            */
        }
    }
}