﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using OCDatalayer;
using OCKarat_82.Common;
using OCKarat_82.Modules.Views;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammGeraeteViewModel
    {
        public virtual INavigationService ContentService { get; set; }
        private readonly GeraetStammdaten _geraeteStammdaten = new GeraetStammdaten();
        public List<Artikel> ListeArtikel { get; set; }
        public List<Geraet> ListeGeraete { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammGeraeteViewModel()
        {
            ListeGeraete = _geraeteStammdaten.GetGeraete();
            Caption = "Gerätestamm";
            IsActive = true;
        }

        public static StammGeraeteViewModel Create()
        {
            return ViewModelSource.Create(() => new StammGeraeteViewModel());
        }


        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Geraet)sender;
            //M13S0402 geraet = _geraeteStammdaten.GetFullGeraetByIdentNr(currentItem.IdentNr);

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents,
                key: "StammGeraeteDetailView" + currentItem.IdentNr))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammGeraeteDetailView" + currentItem.IdentNr, () => StammGeraeteDetailViewModel.Create(currentItem), typeof(StammGeraeteDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammGeraeteDetailView" + currentItem.IdentNr);
            }
        }

        public void ButtonNew()
        {
            Geraet geraet = new Geraet();

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents,
                key: "StammGeraeteDetailViewNeu"))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammGeraeteDetailViewNeu", () => StammGeraeteDetailViewModel.Create(geraet), typeof(StammGeraeteDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammGeraeteDetailViewNeu");
            }
        }

    }
}