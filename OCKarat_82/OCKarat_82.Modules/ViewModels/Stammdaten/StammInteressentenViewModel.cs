﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using OCDatalayer;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammInteressentenViewModel
    {
        private readonly KundenStammdaten _kundenStammdaten = new KundenStammdaten();
        public List<Kunde> ListeInteressenten { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammInteressentenViewModel()
        {
            var kundenStamm = new KundenStammdaten();
            ListeInteressenten = kundenStamm.GetKundenByKennzeichen("I");
            Caption = "Interessenten";
        }

        public static StammInteressentenViewModel Create()
        {
            return ViewModelSource.Create(() => new StammInteressentenViewModel());
        }

        public void NeuButton_Click(object sender)
        {
            /*MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.navFrame.Navigate(new Uri("Views/" + "Stamm" + "/View" + "StammMain" + ".xaml", UriKind.RelativeOrAbsolute), null, false);
            */
        }

        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Kunde)sender;
            // hier started der speziefische Teil für jedes StammdatenViewModel
            A13S0101 interessent = _kundenStammdaten.GetFullKundeById(currentItem.KUNDE);

            /*MainWindow win = (MainWindow)Application.Current.MainWindow;
            object x = GridKunden.SelectedItem;
            win.navFrame.Navigate(new Uri("Views/" + "Stamm" + "/View" + "StammKundeDetail" + ".xaml", UriKind.RelativeOrAbsolute), x, false);
            */
        }


    }
}