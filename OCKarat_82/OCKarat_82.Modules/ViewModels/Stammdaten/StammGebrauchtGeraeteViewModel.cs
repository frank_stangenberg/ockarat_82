﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using OCDatalayer;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammGebrauchtGeraeteViewModel
    {
        public virtual INavigationService ContentService { get; set; }
        private GeraetStammdaten _geraeteStammdaten = new GeraetStammdaten();
        public List<Artikel> ListeArtikel { get; set; }
        public List<Geraet> ListeGeraete { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammGebrauchtGeraeteViewModel()
        {
            ListeGeraete = _geraeteStammdaten.GetGebrauchtGeraete();
            Caption = "Gebrauchtgeräte";
        }

        public static StammGebrauchtGeraeteViewModel Create()
        {
            return ViewModelSource.Create(() => new StammGebrauchtGeraeteViewModel());
        }


        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Geraet)sender;
            //NavigateToView("StammGeraetDetailView", geraet);
        }

        public void NeuesGeraetButtonClick()
        {
            M13S0402 geraet = new M13S0402();
            //NavigateToView("StammGeraetDetailView", geraet);
        }

        public void NavigateToView(string viewName, object param)
        {
            /*
            ContentService = this.GetService<INavigationService>("ContentNavigationService");
             
            object navigateTo = _viewLocator.ResolveView(viewName);
            ContentService.Navigate(navigateTo, param, this);
            */
        }
    }
}