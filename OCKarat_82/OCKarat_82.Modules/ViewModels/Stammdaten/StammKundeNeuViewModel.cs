﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using OCDatalayer;
using OCDatalayer.Stamm;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammKundeNeuViewModel : IEditableObject
    {
        public A13S0101 Kunde { get; set; }
        public List<Stamm> KennzeichenListe { get; set; }

        protected StammKundeNeuViewModel()
        {

        }

        protected StammKundeNeuViewModel(A13S0101 kunde)
        {
            Kunde = kunde;

        }

        public static StammKundeNeuViewModel Create()
        {
            return ViewModelSource.Create(() => new StammKundeNeuViewModel());
        }

        public static StammKundeNeuViewModel Create(A13S0101 kunde)
        {
            return ViewModelSource.Create(() => new StammKundeNeuViewModel(kunde));
        }

        public void BeginEdit()
        {
            throw new NotImplementedException();
        }

        public void EndEdit()
        {
            Kunde = Kunde;
        }

        public void CancelEdit()
        {
            throw new NotImplementedException();
        }

        public void ButtonSave() { EndEdit(); }
        public void Revert() { CancelEdit(); }

        public void btnSave_ClickCommand()
        {

        }
    }
}