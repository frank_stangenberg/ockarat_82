﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using OCDatalayer;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammLieferantenViewModel
    {
        private readonly KundenStammdaten _kundenStammdaten = new KundenStammdaten();
        public List<Kunde> ListeLieferanten { get; set; }
        public string Caption { get; set; }

        protected StammLieferantenViewModel()
        {
            var kundenStamm = new KundenStammdaten();
            ListeLieferanten = kundenStamm.GetKundenByKennzeichen("L");
            Caption = "Lieferantenstamm";
        }

        public static StammLieferantenViewModel Create()
        {
            return ViewModelSource.Create(() => new StammLieferantenViewModel());
        }

        public void NeuButton_Click(object sender)
        {
            /*MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.navFrame.Navigate(new Uri("Views/" + "Stamm" + "/View" + "StammMain" + ".xaml", UriKind.RelativeOrAbsolute), null, false);
            */
        }

        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Kunde)sender;
            // hier started der speziefische Teil für jedes StammdatenViewModel
            // Kunde, Lieferant, Interesent werden alle über die gleiche Tabelle verwaltet
            A13S0101 lieferant = _kundenStammdaten.GetFullKundeById(currentItem.KUNDE);

            /*MainWindow win = (MainWindow)Application.Current.MainWindow;
            object x = GridKunden.SelectedItem;
            win.navFrame.Navigate(new Uri("Views/" + "Stamm" + "/View" + "StammKundeDetail" + ".xaml", UriKind.RelativeOrAbsolute), x, false);
            */
        }
    }
}