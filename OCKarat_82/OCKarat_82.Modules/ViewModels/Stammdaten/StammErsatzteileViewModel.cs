﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using OCDatalayer;
using OCKarat_82.Common;
using OCKarat_82.Modules.Views;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammErsatzteileViewModel
    {
        private readonly ArtikelStammdaten _artikelStammdaten = new ArtikelStammdaten();
        public List<Artikel> ListeArtikel { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammErsatzteileViewModel()
        {
            var artikelStamm = new ArtikelStammdaten();
            ListeArtikel = artikelStamm.GetErsatzteile();
            Caption = "Ersatzteile";
        }

        public static StammErsatzteileViewModel Create()
        {
            return ViewModelSource.Create(() => new StammErsatzteileViewModel());
        }

        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Artikel)sender;
            // hier started der speziefische Teil für jedes StammdatenViewModel
            string identNr = currentItem.Identnummer;
            F00S1201 item = _artikelStammdaten.GetFullArtikelById(identNr);

            ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammArtikelDetailView" + identNr, () => StammArtikelDetailViewModel.Create(item), typeof(StammArtikelDetailView)));
            ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammArtikelDetailView" + identNr);
        }

    }
}