﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using OCDatalayer;
using OCKarat_82.Common;
using OCKarat_82.Modules.Views;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammArtikelViewModel
    {
        private readonly ArtikelStammdaten _artikelStammdaten = new ArtikelStammdaten();
        public List<Artikel> ListeArtikel { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammArtikelViewModel()
        {
            var artikelStamm = new ArtikelStammdaten();
            ListeArtikel = artikelStamm.GetArtikel();
            Caption = "Artikelstamm";
        }

        public static StammArtikelViewModel Create()
        {
            return ViewModelSource.Create(() => new StammArtikelViewModel());
        }

        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Artikel)sender;
            // hier started der speziefische Teil für jedes StammdatenViewModel
            string identNr = currentItem.Identnummer;
            F00S1201 item = _artikelStammdaten.GetFullArtikelById(identNr);

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents, key: "StammArtikelDetailView" + identNr))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammArtikelDetailView" + identNr, () => StammArtikelDetailViewModel.Create(item), typeof(StammArtikelDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammArtikelDetailView" + identNr);
            }
        }

        public void ButtonNew(object sender)
        {
            F00S1201 item = new F00S1201();
            item.Identnr = "NEU";
            string identNr = "Neu";

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents,
                key: "StammArtikelDetailViewNew"))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammArtikelDetailView" + identNr, () => StammArtikelDetailViewModel.Create(item), typeof(StammArtikelDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammArtikelDetailView" + identNr);
            }
        }
    }
}