﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Core;
using OCDatalayer;
using OCDatalayer.Stamm;
using OCKarat_82.Common;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammArtikelDetailViewModel : IEditableObject
    {
        public F00S1201 Artikel { get; set; }

        public List<Stamm> AbcArtikel { get; set; }
        public List<Stamm> ArtikelKennzeichen { get; set; }
        public List<Stamm> GeraeteGruppen { get; set; }
        public List<Stamm> Hersteller { get; set; }
        public List<Stamm> MengenEinheiten { get; set; }
        public List<Stamm> PreisEinheiten { get; set; }
        public List<Stamm> RabattGruppen { get; set; }
        public List<Stamm> VerpackungsEinheiten { get; set; }
        public List<Stamm> WarenGruppen { get; set; }
        public List<Stamm> ListeVerbrauchsschluessel { get; set; }

        public List<ArtikelLieferant> ArtikelLieferanten { get; set; }
        public ArtikelLieferant Lieferant { get; set; }

        // für die Anzeige und Aktivsetzung des Tabs
        public string Caption { get; set; }
        public bool IsActive { get; set; }
        public string CaptionIdentNr { get; set; }

        private readonly ArtikelStammdaten _artikelStammdaten = new ArtikelStammdaten();

        protected StammArtikelDetailViewModel()
        {
            AbcArtikel = OCListen.AbcArtikel;
            ArtikelKennzeichen = OCListen.ArtikelKennzeichen;
            GeraeteGruppen = OCListen.GeraeteGruppen;
            Hersteller = OCListen.Hersteller;
            MengenEinheiten = OCListen.MengenEinheiten;
            PreisEinheiten = OCListen.PreisEinheiten;
            RabattGruppen = OCListen.RabattGruppen;
            VerpackungsEinheiten = OCListen.VerpackungsEinheiten;
            WarenGruppen = OCListen.WarenGruppen;
            ListeVerbrauchsschluessel = OCListen.Verbrauchsschluessel;
        }

        public static StammArtikelDetailViewModel Create(F00S1201 artikel)
        {
            return ViewModelSource.Create(() => new StammArtikelDetailViewModel()
            {
                Artikel = artikel,

                ArtikelLieferanten = (new ArtikelStammdaten()).GetLieferantenByIdentNr(artikel.Identnr),

                CaptionIdentNr = artikel.Identnr,
                Caption = "Artikel: " + artikel.Identnr,
                IsActive = true
            });
        }

        public void LieferantenMouseDoubleClick(object sender)
        {
            var currentItem = (ArtikelLieferant)sender;
            Lieferant = currentItem;
        }

        public void BeginEdit()
        {
            throw new NotImplementedException();
        }

        public void EndEdit()
        {
            if (Artikel.Identnr.ToLower().Equals("neu"))
            {
                // prüfen ob die initiale Identnummer (NEU) geändert wurde.
                DXMessageBox.Show("Bitte ändern Sie den Identnummer.");
            }
            else
            {
                _artikelStammdaten.UpdateArtikel(Artikel);
                ModuleManager.DefaultManager.Remove(regionName: Regions.Documents, key: "StammArtikelDetailView" + CaptionIdentNr, raiseViewModelRemovingEvent: false);
                ModuleManager.DefaultManager.Unregister(regionName: Regions.Documents, key: "StammArtikelDetailView" + CaptionIdentNr);
            }
        }

        public void CancelEdit()
        {
            ModuleManager.DefaultManager.Remove(regionName: Regions.Documents, key: "StammArtikelDetailView" + CaptionIdentNr, raiseViewModelRemovingEvent: false);
            ModuleManager.DefaultManager.Unregister(regionName: Regions.Documents, key: "StammArtikelDetailView" + CaptionIdentNr);
        }

        public void ButtonSave() { ((IEditableObject)this).EndEdit(); }
        public void ButtonCancel() { ((IEditableObject)this).CancelEdit(); }

    }
}