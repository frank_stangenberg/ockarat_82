﻿using System;
using System.Collections.Generic;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.WindowsUI.Navigation;
using OCDatalayer;
using OCKarat_82.Common;
using OCKarat_82.Modules.Views;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammKundenViewModel : ISupportState<StammKundenViewModel.Info>
    {
        private readonly KundenStammdaten _kundenStammdaten = new KundenStammdaten();
        public List<Kunde> ListeKunden { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsActive { get; set; }

        protected StammKundenViewModel()
        {
            var kundenStamm = new KundenStammdaten();
            ListeKunden = kundenStamm.GetKundenByKennzeichen("K");
            Caption = "Kundenstamm";
            IsActive = true;
        }

        public static StammKundenViewModel Create()
        {
            return ViewModelSource.Create(() => new StammKundenViewModel());
        }

        public void TableMouseDoubleClick(object sender)
        {
            var currentItem = (Kunde)sender;
            string kundenId = currentItem.KUNDE;

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents, key: "StammKundenDetailView" + kundenId))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammKundenDetailView" + kundenId, () => StammKundenDetailViewModel.Create(currentItem), typeof(StammKundenDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammKundenDetailView" + kundenId);
            }
        }

        public void ButtonNew(object sender)
        {
            Kunde kunde = new Kunde();
            kunde.KUNDE = "NEU";
            string identNr = "Neu";

            // now inject module
            if (!ModuleManager.DefaultManager.IsInjected(regionName: Regions.Documents,key: "StammKundenDetailView" + identNr))
            {
                ModuleManager.DefaultManager.Register(Regions.Documents, new Module("StammKundenDetailView" + identNr, () => StammKundenDetailViewModel.Create(kunde), typeof(StammKundenDetailView)));
                ModuleManager.DefaultManager.Inject(regionName: Regions.Documents, key: "StammKundenDetailView" + identNr);
            }
        }

        /// <summary>
        /// diese Teil wird verwendet, damit der Zustand, in dem die Applikation sich befindet gespeichert und beim nächsten mal wieder hergestellt werden kann.
        /// </summary>
        #region Serialization
        [Serializable]
        public class Info
        {
            public string Caption { get; set; }
        }
        Info ISupportState<Info>.SaveState()
        {
            return new Info()
            {
                Caption = this.Caption
            };
        }
        void ISupportState<Info>.RestoreState(Info state)
        {
            this.Caption = state.Caption;
        }
        #endregion
    }
}