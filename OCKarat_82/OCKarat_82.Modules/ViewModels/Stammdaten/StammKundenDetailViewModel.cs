﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Core;
using OCDatalayer;
using OCDatalayer.Stamm;
using OCKarat_82.Common;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammKundenDetailViewModel : IEditableObject, ISupportState<StammKundenDetailViewModel.Info>
    {
        public Kunde Kunde { get; set; }

        public List<Stamm> AbcKunde { get; set; }
        public List<Stamm> KundenKennzeichen { get; set; }
        public List<Stamm> Zahlungsbedingungen { get; set; }
        public List<Stamm> Branchen { get; set; }
        public List<Stamm> Bundeslaender { get; set; }
        public List<Stamm> Laender { get; set; }
        public List<Stamm> Regionen { get; set; }
        public List<Stamm> Touren { get; set; }
        public string Caption { get; set; }
        public bool IsActive { get; set; }
        public string CaptionKundeId { get; set; }

        private readonly KundenStammdaten _kundenStammdaten = new KundenStammdaten();
        private readonly StammStammdaten _stammdaten = new StammStammdaten();

        protected StammKundenDetailViewModel()
        {
            AbcKunde = _stammdaten.GetStammListeByPrefix("AB");
            Branchen = _stammdaten.GetStammListeByPrefix("B1");
            Bundeslaender = _stammdaten.GetStammListeByPrefix("BL");
            KundenKennzeichen = _stammdaten.GetStammListeByPrefix("Y3");
            Laender = _stammdaten.GetStammListeByPrefix("31");
            Regionen = _stammdaten.GetStammListeByPrefix("37");
            Touren = _stammdaten.GetStammListeByPrefix("TN");
            Zahlungsbedingungen = _stammdaten.GetStammListeByPrefix("34");
        }

        public static StammKundenDetailViewModel Create(Kunde kunde)
        {
            return ViewModelSource.Create(() => new StammKundenDetailViewModel()
            {
                Kunde = kunde,

                CaptionKundeId = kunde.KUNDE,
                Caption = "Kunde: " + kunde.KUNDE,
                IsActive = true
            });
        }

        public static StammKundenDetailViewModel Create()
        {
            return ViewModelSource.Create(() => new StammKundenDetailViewModel());
        }


        public void BeginEdit()
        {
            throw new NotImplementedException();
        }

        public void EndEdit()
        {
            if (Kunde.KUNDE.ToLower().Equals("neu"))
            {
                // prüfen ob die initiale Identnummer (NEU) geändert wurde.
                DXMessageBox.Show("Bitte ändern Sie Kundennummer.");
            }
            else
            {
                _kundenStammdaten.UpdateKunde(Kunde);
                ModuleManager.DefaultManager.Remove(regionName: Regions.Documents, key: "StammKundenDetailView" + CaptionKundeId, raiseViewModelRemovingEvent: false);
                ModuleManager.DefaultManager.Unregister(regionName: Regions.Documents, key: "StammKundenDetailView" + CaptionKundeId);
            }
        }

        public void CancelEdit()
        {
            ModuleManager.DefaultManager.Remove(regionName: Regions.Documents, key: "StammKundenDetailView" + CaptionKundeId, raiseViewModelRemovingEvent: false);
            ModuleManager.DefaultManager.Unregister(regionName: Regions.Documents, key: "StammKundenDetailView" + CaptionKundeId);
        }

        public void ButtonSave() { ((IEditableObject)this).EndEdit(); }
        public void ButtonCancel() { ((IEditableObject)this).CancelEdit(); }

        /// <summary>
        /// diese Teil wird verwendet, damit der Zustand, in dem die Applikation sich befindet gespeichert und beim nächsten mal wieder hergestellt werden kann.
        /// </summary>
        #region Serialization
        [Serializable]
        public class Info
        {
            public Kunde Kunde { get; set; }
            public string Caption { get; set; }
            public bool IsActive { get; set; }
            public string CaptionKundeId { get; set; }
        }
        Info ISupportState<Info>.SaveState()
        {
            return new Info()
            {
                Kunde = this.Kunde,
                Caption = this.Caption,
                CaptionKundeId = this.CaptionKundeId
            };
        }
        void ISupportState<Info>.RestoreState(Info state)
        {
            this.Kunde = state.Kunde;
            this.Caption = state.Caption;
            this.CaptionKundeId = state.CaptionKundeId;
        }
        #endregion

    }
}