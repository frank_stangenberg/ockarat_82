﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using OCDatalayer;
using OCDatalayer.Stamm;

namespace OCKarat_82.Modules.ViewModels
{
    [POCOViewModel]
    public class StammGeraeteDetailViewModel : IEditableObject
    {
        public Geraet Geraet { get; set; }

        public List<Stamm> GeraeteGruppen { get; set; }
        public List<Stamm> GeraeteStatus { get; set; }
        public List<Stamm> Hersteller { get; set; }
        public List<Stamm> Tonnagen { get; set; }

        // für die Anzeige und Aktivsetzung des Tabs
        public string Caption { get; set; }
        public bool IsActive { get; set; }

        private readonly GeraetStammdaten _geraeteStammdaten = new GeraetStammdaten();
        private readonly StammStammdaten _stammdaten = new StammStammdaten();

        protected StammGeraeteDetailViewModel()
        {
            GeraeteGruppen = _stammdaten.GetStammListeByPrefix("GG");
            GeraeteStatus = _stammdaten.GetStammListeByPrefix("GT");
            Hersteller = _stammdaten.GetStammListeByPrefix("HC");
            Tonnagen = _stammdaten.GetStammListeByPrefix("GA");
        }

        public static StammGeraeteDetailViewModel Create(Geraet geraet)
        {
            return ViewModelSource.Create(() => new StammGeraeteDetailViewModel()
            {
                Geraet = geraet,

                Caption = "Gerät: " + geraet.IdentNr,
                IsActive = true
            });
        }

        /// <summary>Beginnt die Bearbeitung eines Objekts.</summary>
        public void BeginEdit()
        {
            throw new NotImplementedException();
        }


        public void EndEdit()
        {
            _geraeteStammdaten.UpdateGeraet(Geraet);

        }

        public void CancelEdit()
        {
            throw new NotImplementedException();
        }

        public void ButtonSave() { ((IEditableObject)this).EndEdit(); }
        public void ButtonCandel() { ((IEditableObject)this).CancelEdit(); }

    }
}