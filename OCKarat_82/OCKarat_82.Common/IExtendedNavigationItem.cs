﻿using System;

namespace OCKarat_82.Common
{
    public interface IExtendedNavigationItem
    {
        string Caption { get; }
        string GroupName { get; }
        string Key { get; }
        bool UseItem { get; }
        Func<object> ModelFactory { get; }
        Type ViewType { get; }
    }
}
