﻿namespace OCKarat_82.Common
{
    public static class Modules
    {
        public static string Main { get { return "Main"; } }
        public static string StammArtikelView { get { return "StammArtikelView"; } }
        public static string StammGeraeteView { get { return "StammGeraeteView"; } }
        public static string StammInteressentenView { get { return "StammInteressentenView"; } }
        public static string StammKundenView { get { return "StammKundenView"; } }
        public static string StammLieferantenView { get { return "StammLieferantenView"; } }
        public static string StammMitarbeiterView { get { return "StammMitarbeiterView"; } }

    }
}
