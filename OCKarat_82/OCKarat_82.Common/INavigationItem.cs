﻿using System;

namespace OCKarat_82.Common
{
    public interface INavigationItem
    {
        string Caption { get; }
        string GroupName { get; }
    }
}
